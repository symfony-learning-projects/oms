<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200915143328 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pays (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statistique (id INT AUTO_INCREMENT NOT NULL, virus_id INT NOT NULL, pays_id INT NOT NULL, created_at DATETIME NOT NULL, contaminated INT NOT NULL, recovered INT NOT NULL, zombie INT NOT NULL, INDEX IDX_73A038AD86619E6D (virus_id), INDEX IDX_73A038ADA6E44244 (pays_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE virus (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(9) NOT NULL, index_danger INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE statistique ADD CONSTRAINT FK_73A038AD86619E6D FOREIGN KEY (virus_id) REFERENCES virus (id)');
        $this->addSql('ALTER TABLE statistique ADD CONSTRAINT FK_73A038ADA6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE statistique DROP FOREIGN KEY FK_73A038ADA6E44244');
        $this->addSql('ALTER TABLE statistique DROP FOREIGN KEY FK_73A038AD86619E6D');
        $this->addSql('DROP TABLE pays');
        $this->addSql('DROP TABLE statistique');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE virus');
    }
}
