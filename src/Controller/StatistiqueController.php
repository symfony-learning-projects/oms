<?php

namespace App\Controller;

use App\Entity\Statistique;
use App\Form\StatistiqueType;
use App\Repository\StatistiqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/statistique")
 * @IsGranted("ROLE_USER")
 */
class StatistiqueController extends AbstractController
{
    /**
     * @Route("/", name="statistique_index", methods={"GET"})
     */
    public function index(StatistiqueRepository $statistiqueRepository): Response
    {
        return $this->render('statistique/index.html.twig', [
            'statistiques' => $statistiqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="statistique_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $statistique = new Statistique();
        $form = $this->createForm(StatistiqueType::class, $statistique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($statistique);
            $entityManager->flush();

            return $this->redirectToRoute('statistique_index');
        }

        return $this->render('statistique/new.html.twig', [
            'statistique' => $statistique,
            'form' => $form->createView(),
        ]);
    }

}
