<?php

namespace App\Controller;

use App\Repository\PaysRepository;
use App\Repository\StatistiqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/")
 */
class AppController extends AbstractController{
    
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(PaysRepository $paysRepository): Response{

        return $this->render('index.html.twig', [
            'controller_name' => 'AppController',
            'allPays' => $paysRepository->findAll(),
        ]);
    }
}
