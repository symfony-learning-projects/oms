<?php

namespace App\Form;

use App\Entity\Virus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VirusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'Nom',
                ],
                'label' => 'Nom'
            ])
            ->add('indexDanger', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'Indice de dangerosité',
                    'min' => 1,
                    'max' => 30
                ],
                'label' => 'Indice de dangerosité'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Virus::class,
        ]);
    }
}
