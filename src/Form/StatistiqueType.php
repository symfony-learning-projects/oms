<?php

namespace App\Form;

use App\Entity\Pays;
use App\Entity\Statistique;
use App\Entity\Virus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatistiqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdAt', DateTimeType::class, [
                'widget' => 'single_text',
                'label' => 'Date de création',
            ])
            ->add('contaminated', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'Contaminés',
                    'min' => 0,
                ],
                'label' => 'Nombre de contaminés'
            ])
            ->add('recovered', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'Guéris',
                    'min' => 0,
                ],
                'label' => 'Nombre de guéris'
            ])
            ->add('zombie', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'Zombies',
                    'min' => 0,
                ],
                'label' => 'Nombre de zombies'
            ])
            ->add('virus', EntityType::class, [
                'class' => Virus::class,
                'label' => 'Virus',
                'choice_label' => 'name',
            ])
            ->add('pays', EntityType::class, [
                'class' => Pays::class,
                'label' => 'Pays',
                'choice_label' => 'name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Statistique::class,
        ]);
    }
}
