<?php

namespace App\Entity;

use App\Repository\PaysRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PaysRepository::class)
 */
class Pays
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le nom ne peut pas être vide.")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Statistique::class, mappedBy="pays")
     */
    private $statistiques;

    public function __construct()
    {
        $this->statistiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function totalContaminated(): int{
        $somme = 0;
        foreach ($this->getStatistiques() as $value){
            $somme = $somme + $value->getContaminated();
        }
        return $somme;
    }

    public function totalRecovered(): int{
        $somme = 0;
        foreach ($this->getStatistiques() as $value){
            $somme = $somme + $value->getRecovered();
        }
        return $somme;
    }

    public function totalZombie(): int{
        $somme = 0;
        foreach ($this->getStatistiques() as $value){
            $somme = $somme + $value->getZombie();
        }
        return $somme;
    }

    public function lastStatistique(): Statistique{
        $lastStatistique = new Statistique();
        foreach ($this->statistiques as $value){
            if ($lastStatistique->getCreatedAt() < $value->getCreatedAt()){
                $lastStatistique = $value;
            }
        }
        return $lastStatistique;
    }

    /**
     * @return Collection|Statistique[]
     */
    public function getStatistiques(): Collection
    {
        return $this->statistiques;
    }

    public function addStatistique(Statistique $statistique): self
    {
        if (!$this->statistiques->contains($statistique)) {
            $this->statistiques[] = $statistique;
            $statistique->setPays($this);
        }

        return $this;
    }

    public function removeStatistique(Statistique $statistique): self
    {
        if ($this->statistiques->contains($statistique)) {
            $this->statistiques->removeElement($statistique);
            // set the owning side to null (unless already changed)
            if ($statistique->getPays() === $this) {
                $statistique->setPays(null);
            }
        }

        return $this;
    }
}
