<?php

namespace App\Entity;

use App\Repository\VirusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=VirusRepository::class)
 */
class Virus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=9)
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le nom ne peut pas être vide.")
     * @Assert\Regex(
     *     pattern="/(([A-Z]{4})\-([0-9]{4})){1}/",
     *     message="Le nom doit être au format ABCD-1234"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\NotBlank(message="L'indice de danger ne peut pas être vide.")
     * @Assert\Range(
     *      min = 1,
     *      max = 30,
     *      notInRangeMessage = "L'index de danger doit être compris entre {{ min }} et {{ max }}")
     */
    private $indexDanger;

    /**
     * @ORM\OneToMany(targetEntity=Statistique::class, mappedBy="virus")
     */
    private $statistiques;

    public function __construct()
    {
        $this->statistiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIndexDanger(): ?int
    {
        return $this->indexDanger;
    }

    public function setIndexDanger(int $indexDanger): self
    {
        $this->indexDanger = $indexDanger;

        return $this;
    }

    /**
     * @return Collection|Statistique[]
     */
    public function getStatistiques(): Collection
    {
        return $this->statistiques;
    }

    public function addStatistique(Statistique $statistique): self
    {
        if (!$this->statistiques->contains($statistique)) {
            $this->statistiques[] = $statistique;
            $statistique->setVirus($this);
        }

        return $this;
    }

    public function removeStatistique(Statistique $statistique): self
    {
        if ($this->statistiques->contains($statistique)) {
            $this->statistiques->removeElement($statistique);
            // set the owning side to null (unless already changed)
            if ($statistique->getVirus() === $this) {
                $statistique->setVirus(null);
            }
        }

        return $this;
    }
}
