<?php

namespace App\DataFixtures;

use App\Entity\Pays;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PaysFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $pays1 = new Pays();
        $pays1->setName('France');
        $manager->persist($pays1);

        $pays2 = new Pays();
        $pays2->setName('Italie');
        $manager->persist($pays2);

        $pays3 = new Pays();
        $pays3->setName('Allemagne');
        $manager->persist($pays3);

        $pays4 = new Pays();
        $pays4->setName('Espagne');
        $manager->persist($pays4);

        $pays5 = new Pays();
        $pays5->setName('Royaume-Uni');
        $manager->persist($pays5);

        $manager->flush();
    }
}
